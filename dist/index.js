'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PgConsumer = function () {
    function PgConsumer(log, db, client, dataTable, offsetTable) {
        var batchSize = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 100;

        _classCallCheck(this, PgConsumer);

        this.log = log;
        this.db = db;
        this.client = client;
        this.batchSize = batchSize;
        this.dataTable = dataTable;
        this.offsetTable = offsetTable;
    }

    _createClass(PgConsumer, [{
        key: 'consume',
        value: function consume(topic, action) {
            var _this = this;

            var interval = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 5000;

            return new Promise(function (resolve, reject) {
                _this._consume(topic, action).then(function () {
                    setTimeout(function () {
                        _this.consume(topic, action, interval);
                    }, interval);
                }).catch(function (e) {
                    setTimeout(function () {
                        _this.consume(topic, action, interval);
                    }, interval);
                    reject(e);
                });
            });
        }
    }, {
        key: '_consume',
        value: function _consume(topic, onUplink) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                _this2.db.any('select uplink_id from ' + _this2.offsetTable + ' where client=\'' + _this2.client + '\' and topic=\'' + topic + '\';').then(function (data) {
                    if (data.length == 0) {
                        // No offset
                        var query = 'select u.id as id, u.payload as payload from ' + _this2.dataTable + ' u where u.topic=\'' + topic + '\' order by u.id';
                        _this2._read_and_commit(query, topic, onUplink).then(function () {
                            resolve();
                        }).catch(function (e) {
                            reject(e);
                        });
                    } else {
                        // Read from offset
                        var _query = 'select u.id as id, u.payload as payload from ' + _this2.dataTable + ' u join ' + _this2.offsetTable + ' o on (u.topic = o.topic) where o.client=\'' + _this2.client + '\' and o.topic=\'' + topic + '\' and u.id > o.uplink_id order by u.id';

                        _this2._read_and_commit(_query, topic, onUplink).then(function () {
                            resolve();
                        }).catch(function (e) {
                            reject(e);
                        });
                    }
                }).catch(function (e) {
                    _this2.log.info('Failed to read offset', e);
                    reject(e);
                });
            });
        }
    }, {
        key: 'resetOffset',
        value: function resetOffset(topic) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
                _this3.db.none('delete from ' + _this3.offsetTable + ' where client=\'' + _this3.client + '\' and topic=\'' + topic + '\';').then(function () {
                    resolve();
                }).catch(function (e) {
                    reject('failed to reset offset', e);
                });
            });
        }
    }, {
        key: '_commit',
        value: function _commit(topic, offset) {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
                _this4.log.info('committing ' + _this4.client + '@' + topic + '#' + offset);
                var query = 'insert into ' + _this4.offsetTable + '(client, topic, uplink_id) values(\'' + _this4.client + '\', \'' + topic + '\', ' + offset + ') on conflict on constraint offsets_pkey do update set uplink_id=' + offset + ';';
                //this.log.info(query);
                _this4.db.none(query).then(function () {
                    resolve();
                }).catch(function (e) {
                    _this4.log.info("failed to commit offset");
                    reject(e);
                });
            });
        }
    }, {
        key: '_read_and_commit',
        value: function _read_and_commit(query, topic, onUplink) {
            var _this5 = this;

            return new Promise(function (resolve, reject) {
                _this5.db.any(query + (_this5.batchSize > 0 ? ' limit ' + _this5.batchSize + ';' : ';')).then(function (result) {
                    if (result.length > 0) {
                        _this5.log.info('processing ' + result.length + ' uplinks for topic ' + topic);
                        onUplink(result.map(function (r) {
                            return r.payload;
                        })).then(function () {
                            var latestOffset = Math.max.apply(Math, result.map(function (o) {
                                return o.id;
                            }));
                            _this5._commit(topic, latestOffset).then(function () {
                                resolve('offset ' + latestOffset + ' committed');
                            }).catch(function (e) {
                                _this5.log.info('failed to update offsets');
                                reject(e);
                            });
                        }).catch(function (e) {
                            _this5.log.warn(e);
                            var latestOffset = Math.max.apply(Math, result.map(function (o) {
                                return o.id;
                            }));
                            _this5._commit(topic, latestOffset).then(function () {
                                resolve('offset ' + latestOffset + ' committed');
                            }).catch(function (e) {
                                _this5.log.info('failed to update offsets');
                                reject(e);
                            });
                        });
                    } else {
                        resolve('no data to consume');
                    }
                }).catch(function (e) {
                    _this5.log.info("failed to get uplink data");
                    reject(e);
                });
            });
        }
    }]);

    return PgConsumer;
}();

exports.default = PgConsumer;