# pg-consumer

Kafka-like usage of categorized message streams in Postgres

## Prerequisites

Two tables are needed in Postgres, one to store incoming uplink data and one to store consumer clients offsets

```sql
CREATE TABLE IF NOT EXISTS uplink
(
    id SERIAL UNIQUE,
    topic TEXT,
    payload JSON,
    CONSTRAINT id_pkey PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS offsets
(
    client TEXT,
    topic TEXT,
    uplink_id INTEGER REFERENCES uplink(id),
    CONSTRAINT offsets_pkey PRIMARY KEY (client, topic)
);
```

## Usage

```javascript
const pgConsumer = new PgConsumer(log, pgPromise, 'myConsumerClient', 'uplink', 'offsets');

pgConsumer.consume('myTopic', (uplinks) => {
    uplinks.forEach((uplink) => {
        console.log(uplink);
    });
}));
```