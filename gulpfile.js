let gulp = require('gulp');
let del = require('del');
let bump = require('gulp-bump');
let eslint = require('gulp-eslint');
let babel = require('gulp-babel');

let paths = {
    scripts: ['src/**/*.js'],
    static: []
};


// ---- preparations ----
gulp.task('clean', () => {
    return del(['build', 'dist']);
});

gulp.task('lint', () => {
    return gulp.src(paths.scripts)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});


// ---- build ----
gulp.task('build:prep', ['clean', 'lint']);

gulp.task('build:scripts', ['build:prep'], () => {
    return gulp.src(paths.scripts)
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('build:static', ['build:prep'], () => {
    return gulp.src(paths.static)
        .pipe(gulp.dest('dist'));
});

gulp.task('build', ['build:static', 'build:scripts']);

gulp.task('bump', (cb) => {
    return gulp.src('./package.json')
        .pipe(bump())
        .pipe(gulp.dest('.'));
});
gulp.task('bump:minor', (cb) => {
    return gulp.src('./package.json')
        .pipe(bump({type: 'minor'}))
        .pipe(gulp.dest('.'));
});
gulp.task('bump:major', (cb) => {
    return gulp.src('./package.json')
        .pipe(bump({type: 'major'}))
        .pipe(gulp.dest('.'));
});


gulp.task('default', ['build']);