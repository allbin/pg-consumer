class PgConsumer {
    constructor(log, db, client, dataTable, offsetTable, batchSize = 100) {
        this.log = log;
        this.db = db;
        this.client = client;
        this.batchSize = batchSize;
        this.dataTable = dataTable;
        this.offsetTable = offsetTable;
    }

    consume(topic, action, interval = 5000) {
        return new Promise((resolve, reject) => {
            this._consume(topic, action)
            .then(() => {
                setTimeout(() => { this.consume(topic, action, interval); }, interval);
            })
            .catch((e) => {
                setTimeout(() => { this.consume(topic, action, interval); }, interval);
                reject(e);
            });
        });
    }

    _consume(topic, onUplink) {
        return new Promise((resolve, reject) => {
            this.db.any(`select uplink_id from ${this.offsetTable} where client='${this.client}' and topic='${topic}';`)
            .then((data) => {
                if (data.length == 0) {
                    // No offset
                    const query = `select u.id as id, u.payload as payload from ${this.dataTable} u where u.topic='${topic}' order by u.id`;
                    this._read_and_commit(query, topic, onUplink)
                        .then(() => {
                            resolve();
                        })
                        .catch((e) => {
                            reject(e);
                        });

                } else {
                    // Read from offset
                    const query = `select u.id as id, u.payload as payload from ${this.dataTable} u join ${this.offsetTable} o on (u.topic = o.topic) where o.client='${this.client}' and o.topic='${topic}' and u.id > o.uplink_id order by u.id`;

                    this._read_and_commit(query, topic, onUplink)
                        .then(() => {
                            resolve();
                        })
                        .catch((e) => {
                            reject(e);
                        });
                }
            })
            .catch((e) => {
                this.log.info('Failed to read offset', e);
                reject(e);
            });
        });
    }

    resetOffset(topic) {
        return new Promise((resolve, reject) => {
            this.db.none(`delete from ${this.offsetTable} where client='${this.client}' and topic='${topic}';`)
            .then(() => { resolve(); })
            .catch((e) => {
                reject('failed to reset offset', e);
            });
        });
    }

    _commit(topic, offset) {
        return new Promise((resolve, reject) => {
            this.log.info(`committing ${this.client}@${topic}#${offset}`);
            let query = `insert into ${this.offsetTable}(client, topic, uplink_id) values('${this.client}', '${topic}', ${offset}) on conflict on constraint offsets_pkey do update set uplink_id=${offset};`;
            //this.log.info(query);
            this.db.none(query)
                .then(() => {
                    resolve();
                })
                .catch((e) => {
                    this.log.info("failed to commit offset");
                    reject(e);
                });

        });
    }

    _read_and_commit(query, topic, onUplink) {
        return new Promise((resolve, reject) => {
            this.db.any(query +  (this.batchSize > 0 ? ` limit ${this.batchSize};` : ';'))
                .then((result) => {
                    if (result.length > 0) {
                        this.log.info(`processing ${result.length} uplinks for topic ${topic}`);
                        onUplink(result.map((r) => { return r.payload; }))
                            .then(() => {
                                let latestOffset = Math.max.apply(Math, result.map((o) => {return o.id;}));
                                this._commit(topic, latestOffset)
                                    .then(() => {
                                        resolve(`offset ${latestOffset} committed`);
                                    })
                                    .catch((e) => {
                                        this.log.info('failed to update offsets');
                                        reject(e);
                                    });
                            })
                            .catch((e) => {
                                this.log.warn(e);
                                let latestOffset = Math.max.apply(Math, result.map((o) => {return o.id;}));
                                this._commit(topic, latestOffset)
                                    .then(() => {
                                        resolve(`offset ${latestOffset} committed`);
                                    })
                                    .catch((e) => {
                                        this.log.info('failed to update offsets');
                                        reject(e);
                                    });

                            });
                    } else {
                        resolve(`no data to consume`);
                    }
                })
                .catch((e) => {
                    this.log.info("failed to get uplink data");
                    reject(e);
                });
            });
    }
}

export default PgConsumer;